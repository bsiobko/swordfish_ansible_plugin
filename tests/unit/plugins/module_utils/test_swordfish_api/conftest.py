from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import pytest
import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
from ansible_collections.spbstu.swordfish.tests.unit.compat.mock import MagicMock
from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RestClient
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.constants import \
    OPEN_URL_FUNC


@pytest.fixture
def rest():
    return RestClient(
        base_url='localhost',
        username='Administrator',
        password='Password'
    )


@pytest.fixture
def make_mock(mocker):
    def f(target, return_value=None, side_effect=None, chain_calls=False):
        if return_value is None:
            return_value = {}

        if target == OPEN_URL_FUNC:
            return_value = _mock_open_url(return_value, chain_calls)

        mock = mocker.patch(
            target,
            side_effect=side_effect,
            return_value=return_value,
        )

        return mock

    def _mock_open_url(return_value, chain_calls):
        response_mock = MagicMock()
        if chain_calls and return_value is not None:
            gen = (json.dumps(d) for d in return_value)
            response_mock.read = gen.__next__ \
                if hasattr(gen, '__next__') else gen.next
        else:
            response_mock.read.return_value = json.dumps(return_value)
        return response_mock

    return f


@pytest.fixture
def open_url_kwargs():
    return {
        "method": 'GET',
        "url": "https://localhost",
        "data": None,
        "validate_certs": False,
        "use_proxy": True,
        "timeout": 60,
        "follow_redirects": "all",
        "headers": {},
        "force_basic_auth": False,
    }


@pytest.fixture()
def storage_collection():
    return {
        "@odata.id": "/redfish/v1/Storage",
        "@odata.type": "#StorageCollection.StorageCollection",
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
            }
        ],
        "Members@odata.count": 2,
        "Name": "Storage Collection"
    }


@pytest.fixture()
def storage_data():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved.",
        "StoragePool": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools"
        }
    }


@pytest.fixture()
def storage_pools_collection_data():
    return {
        "@Redfish.Copyright": "Copyright 2014-2021 SNIA. All rights reserved.",
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools",
        "@odata.type": "#StoragePoolCollection.StoragePoolCollection",
        "Members": [
            {"@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool"}
        ],
        "Members@odata.count": 1,
        "Name": "StoragePool Collection"
    }


@pytest.fixture
def drives_groups_data():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers"
                       " and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "StoragePool": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools"
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
    }


@pytest.fixture
def storage_pools_data():
    return [{
        "@Redfish.Copyright": "Copyright 2IPAttachedDrive1TestPool4-2IPAttachedDrive12TestPool SNIA. All rights reserved.",
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool",
        "@odata.type": "#StoragePool.vTestPool_7_TestPool.StoragePool",
        "Capacity": {
            "Data": {
                "ProvisionedBytes": 10000
            }
        },
        "Description": "pool for test",
        "Id": "TestPool",
        "Name": "TestPool"
    }]


@pytest.fixture
def drive_data():
    return {
        "@odata.type": "#Drive.v1_15_0.Drive",
        "Name": "NVMe IPAttachedDrive Drive",
        "Id": "IPAttachedDrive",
        "BlockSizeBytes": 512,
        "CapableSpeedGbs": 12,
        "CapacityBytes": 899527000000,
        "Description": "IP Attached drive.",
        "EncryptionAbility": "None",
        "FailurePredicted": False,
        "Identifiers": [
            {
                "DurableNameFormat": "NAA",
                "DurableName": "500003942810D13A"
            }
        ],
        "LocationIndicatorActive": True,
        "MediaType": "SSD",
        "Manufacturer": "Contoso",
        "Model": "ST9146802SS",
        "NegotiatedSpeedGbs": 12,
        "PartNumber": "SG0GP8811253178M02GJA00",
        "PhysicalLocation": {
            "PartLocation": {
                "LocationType": "Slot"
            }
        },
        "PredictedMediaLifeLeftPercent": 86,
        "Protocol": "NVMe",
        "Revision": "S20A",
        "SKU": "N/A",
        "SerialNumber": "72D0A037FRD26",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "StatusIndicator": "OK",
        "WriteCacheEnabled": True,
        "Links": {
            "Volumes": [
                {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                }
            ],
            "NetworkDeviceFunctions": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/NetworkAdapters/8fd725a1/"
                                 "NetworkDeviceFunctions/11100"
                },
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/NetworkAdapters/8fd725a1/"
                                 "NetworkDeviceFunctions/11101"
                }
            ]
        },
        "Actions": {
            "#Drive.Reset": {
                "target": "/redfish/v1/Chassis/IPAttachedDrive/Drives/IPAttachedDrive1/Actions/Drive.Reset"
            },
            "#Drive.SecureErase": {
                "target": "/redfish/v1/Chassis/IPAttachedDrive/Drives/IPAttachedDrive1/Actions/Drive.SecureErase"
            }
        },
        "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
    }


@pytest.fixture()
def storage_volume_collection():
    return {
        "@odata.type": "#VolumeCollection.VolumeCollection",
        "Name": "Storage Volume Collection",
        "Description": "Storage Volume Collection",
        "Members@odata.count": 3,
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
            }
        ],
        "Oem": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
    }


@pytest.fixture()
def volume_data():
    return {
        "@odata.type": "#Volume.v1_8_0.Volume",
        "Id": "1",
        "Name": "Namespace 1",
        "LogicalUnitNumber": 1,
        "Description": "A Namespace is a quantity of non-volatile memory that may be formatted into logical blocks."
                       " When formatted, a namespace of size n is a collection of logical blocks with logical block"
                       " addresses from 0 to (n-1). NVMe systems can support multiple namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159029384"
            }
        ],
        "Capacity": {
            "Data": {
                "ConsumedBytes": 0,
                "AllocatedBytes": 10737418240
            }
        },
        "NVMeNamespaceProperties": {
            "NamespaceId": "0x011",
            "NamespaceFeatures": {
                "SupportsThinProvisioning": False,
                "SupportsAtomicTransactionSize": False,
                "SupportsDeallocatedOrUnwrittenLBError": False,
                "SupportsNGUIDReuse": False,
                "SupportsIOPerformanceHints": False
            },
            "NumberLBAFormats": 0,
            "FormattedLBASize": "LBAFormat0Support",
            "MetadataTransferredAtEndOfDataLBA": False,
            "NVMeVersion": "1.4"
        },
        "OptimumIOSizeBytes": 1024,
        "Links": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
    }


@pytest.fixture()
def storage_ip_attached_drive1():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
    }
