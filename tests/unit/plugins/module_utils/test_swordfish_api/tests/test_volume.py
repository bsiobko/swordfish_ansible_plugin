from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import pytest

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestVolume:
    def test_get_volume(self, rest, make_mock, storage_ip_attached_drive1, volume_data):
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        volume = storage.get_volume("SimpleNamespace")
        assert isinstance(volume, Volume)
        expected_volume = Volume.from_json(client=rest, data=volume_data)
        assert volume.get_name() == expected_volume.get_name()
        assert volume.get_id() == expected_volume.get_id()
        assert volume.status == expected_volume.status
        assert volume.description == expected_volume.description

    def test_get_volume_not_found(self, rest, make_mock, storage_ip_attached_drive1):
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        make_mock(
            target=OPEN_URL_FUNC,
            side_effect=RESTClientError,
        )
        with pytest.raises(RESTClientError):
            storage.get_volume("SimpleNamespace-fake")

    def test_create_volume(self, rest, make_mock, storage_ip_attached_drive1, volume_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        create_volume_id = "test-volume"
        path = "{0}/Volumes/{1}".format(storage.get_path(), create_volume_id)

        storage.create_volume(volume_id=create_volume_id, data=None)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            data=None
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_create_volume_with_body(self, rest, make_mock, storage_ip_attached_drive1, volume_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        volume_id = "test-volume"
        path = "{0}/Volumes/{1}".format(storage.get_path(), volume_id)

        body_data = {"Manufacturer": "NVMeDriveVendorFoo"}
        json_patch_data = json.dumps(body_data)
        storage.create_volume(volume_id=volume_id, data=body_data)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_delete_volume(self, rest, make_mock, storage_ip_attached_drive1, volume_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        delete_volume_id = "test-volume"
        path = "{0}/Volumes/{1}".format(storage.get_path(), delete_volume_id)

        storage.delete_volume(volume_id=delete_volume_id)
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}".format(path),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_patch_volume(self, rest, make_mock, storage_ip_attached_drive1, volume_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)
        volume_id = "test-volume"
        path = "{0}/Volumes/{1}".format(storage.get_path(), volume_id)

        patch_data = {"Manufacturer": "NVMeDriveVendorFoo"}
        json_patch_data = json.dumps(patch_data)
        storage.patch_volume(volume_id=volume_id, data=patch_data)
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
