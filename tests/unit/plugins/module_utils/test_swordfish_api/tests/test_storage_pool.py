from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import pytest
from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RestClient
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.constants import \
    OPEN_URL_FUNC
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestStoragePools:

    def test_get_storage_pools_from_storage(
            self,
            rest,
            make_mock,
            storage_data,
            storage_pools_data,
            storage_pools_collection_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pools_collection_data, storage_pools_data[0]],
            chain_calls=True
        )
        storage = rest.get_storage("IPAttachedDrive1")

        pools = storage.get_storage_pools()
        pool = pools[0]
        assert pool.name == 'TestPool'
        assert pool.id == 'TestPool'
        assert pool.description == 'pool for test'
        assert pool.odata_id == '/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool'
