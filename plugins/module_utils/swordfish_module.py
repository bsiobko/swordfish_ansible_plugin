from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible.module_utils.basic import AnsibleModule

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTAuthorizationError
from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RestClient


class SwordfishModule(AnsibleModule):

    def __init__(
            self,
            argument_spec=None,
            supports_check_mode=False,
            required_if=None,
            required_one_of=None,
            mutually_exclusive=None,
    ):

        _argument_spec = dict(
            connection=dict(
                required=True,
                type="dict",
                options=dict(
                    base_url=dict(required=True, type="str"),
                    username=dict(required=False, type="str"),
                    password=dict(
                        required=False,
                        type="str",
                        no_log=True,
                    )
                )
            )
        )

        if argument_spec and isinstance(argument_spec, dict):
            _argument_spec.update(argument_spec)

        super(SwordfishModule, self).__init__(
            argument_spec=_argument_spec,
            supports_check_mode=supports_check_mode,
            required_if=required_if,
            required_one_of=required_one_of,
            mutually_exclusive=mutually_exclusive
        )

        connection = self.params['connection']

        self.rest = RestClient(
            base_url=connection['base_url'],
            username=connection['username'],
            password=connection['password']
        )
        try:
            self.rest.authorize()
            self.run()
        except RESTAuthorizationError as e:
            self.fail_json(
                msg='Authorization failed',
                error='{0}: {1}'.format(type(e).__name__, e),
            )
        except Exception as e:
            self.fail_json(
                msg='Operation failed',
                error='{0}: {1}'.format(type(e).__name__, e),
            )

    # Вызывается во время запуска модуля.
    def run(self):
        raise NotImplementedError('Method not implemented!')

    # Используется для вывода результатов выполнения модуля.
    def exit_json(self, **kwargs):
        self._logout()
        super(SwordfishModule, self).exit_json(**kwargs)

    # Если в процессе выполнения модуля произошло что-то не так, то
    # вызываем fail_json и передаем сообщение, которое будет выведено пользователю.
    def fail_json(self, **kwargs):
        self._logout()
        super(SwordfishModule, self).fail_json(**kwargs)

    def _logout(self):
        try:
            self.rest.logout()
        except Exception as e:
            self.warn(
                'Logout failed. {0}: {1}'.format(type(e).__name__, e)
            )
