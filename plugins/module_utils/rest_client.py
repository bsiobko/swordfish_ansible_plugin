from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.fabric import Fabric
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller_collection import \
    StorageControllerCollection

try:
    from typing import Optional, List
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = List = None

from ansible.module_utils.urls import open_url
from ansible.module_utils.six.moves.urllib.parse import urlencode
from ansible.module_utils.six.moves.urllib.error import HTTPError
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    RESTClientError,
    RESTAuthorizationError,
    RESTClientNotFoundError,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.drive_group import DriveGroup
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import (
    SESSION_ENDPOINT,
    STORAGE_ENDPOINT,
    ROOT_ENDPOINT,
    STORAGE_POOLS_ENDPOINT,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.manager.manager import Manager
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.account.service import AccountService


class RestClient:
    def __init__(
            self,
            base_url,
            username=None,
            password=None,
            validate_certs=False,
            timeout=60,
    ):
        self._username = username
        self._password = password
        self._token = None
        self._session = None
        self._cookies = ""
        if "://" in base_url:
            self._protocol, self._host = base_url.split("://")
        else:
            self._protocol = "https"
            self._host = base_url
        self.validate_certs = validate_certs
        self.timeout = timeout

    def authorize(self, username=None, password=None, auth_method=None):
        self._username = username or self._username
        self._password = password or self._password

        try:
            response = self.post(
                SESSION_ENDPOINT,
                body={'UserName': self._username, 'Password': self._password},
            ).headers
        except Exception as e:
            raise RESTAuthorizationError('{0}: {1}'.format(
                type(e).__name__, str(e)
            ))

        self._token = response['X-Auth-Token']
        self._session = response['Location']
        for cookie in response.get_all('Set-Cookie'):
            self._cookies += cookie + ';'

    def make_request(
            self,
            path,
            method,
            query_params=None,
            body=None,
            headers=None,
    ):

        request_kwargs = {
            "follow_redirects": "all",
            "force_basic_auth": False,
            "headers": self._get_headers(),
            "method": method,
            "timeout": self.timeout,
            "use_proxy": True,
            "validate_certs": self.validate_certs,
        }
        if body:
            if isinstance(body, dict) or isinstance(body, list):
                request_kwargs["headers"]["Content-Type"] = "application/json"
                request_body = json.dumps(body)
            elif isinstance(body, bytes):
                request_kwargs["headers"]["Content-Type"] = "application/octet-stream"
                request_body = body
            else:
                raise RESTClientError(
                    "Unsupported body type: {0}".format(type(body)))
        else:
            request_body = None

        url = "{0}/{1}".format(self.base_url.rstrip("/"), path.lstrip("/"))
        if query_params:
            url += "?" + urlencode(query_params)

        if headers:
            request_kwargs["headers"].update(headers)

        try:
            response = open_url(url=url, data=request_body, **request_kwargs)
        except HTTPError as e:
            if e.code == 404:
                raise RESTClientNotFoundError("{0} not found.".format(e.code))
            else:
                raise RESTClientError("Bad request: {0}".format(e.code))
        return response

    def get(self, path, query_params=None, headers=None):
        return self.make_request(path, method="GET", query_params=query_params, headers=headers)

    def post(self, path, body=None, headers=None):
        return self.make_request(path, method="POST", body=body, headers=headers)

    def patch(self, path, body=None, headers=None):
        return self.make_request(path, method="PATCH", body=body, headers=headers)

    def delete(self, path, headers=None):
        return self.make_request(path, method="DELETE", headers=headers)

    def put(self, path, body=None, headers=None):
        return self.make_request(
            path, method="PUT", body=body, headers=headers)

    def _get_headers(self):
        headers = {}
        if self._token:
            headers['X-Auth-Token'] = self._token
            headers['Cookie'] = self._cookies
        return headers

    @property
    def base_url(self):
        return "{0}://{1}".format(self._protocol, self._host)

    def logout(self):  # type: () -> None
        if self._token:
            self.delete(self._session)
            self._token = None
            self._session = None

    def get_drive_groups(self):
        rv = []
        drive_groups_data = json.loads(self.get(STORAGE_ENDPOINT).read())['Members']
        for data in drive_groups_data:
            rv.append(DriveGroup(client=self, path=data['@odata.id']))
        return rv

    def get_manager_collection(self):  # type: () -> List[Manager]
        managers = []
        managers_collection = json.loads(self.get("{0}/Managers".format(ROOT_ENDPOINT)).read())
        for member in managers_collection["Members"]:
            manager_data = self.get(member["@odata.id"]).json
            managers.append(Manager.from_json(self, manager_data))
        return managers

    def get_manager(self, manager_id):  # type: (str) -> Optional[Manager]
        if not isinstance(manager_id, str):
            raise TypeError("Manager id must be string. Received: {0}".format(type(manager_id)))

        manager_path = "{0}/Managers/{1}".format(ROOT_ENDPOINT, manager_id)
        try:
            manager_data = json.loads(self.get(manager_path).read())
        except RESTClientNotFoundError:
            return None
        return Manager.from_json(self, manager_data)

    def get_storage(self, storage_id):  # type: (str) -> Optional[Storage]
        if not isinstance(storage_id, str):
            raise TypeError("Storage id must be string. Received: {0}".format(type(storage_id)))
        storage_path = "{0}/Storage/{1}".format(ROOT_ENDPOINT, storage_id)
        try:
            storage_data = json.loads(self.get(storage_path).read())
        except RESTClientNotFoundError:
            return None
        return Storage.from_json(self, storage_data)

    def get_fabric(self, fabric_id):  # type: (str) -> Optional[Fabric]
        if not isinstance(fabric_id, str):
            raise TypeError("Fabric id must be string. Received: {0}".format(type(fabric_id)))
        fabric_path = "{0}/Fabrics/{1}".format(ROOT_ENDPOINT, fabric_id)
        try:
            fabric_data = json.loads(self.get(fabric_path).read())
        except RESTClientNotFoundError:
            return None
        return Fabric.from_json(self, fabric_data)

    def get_storage_ctrl_collection(self, storage_id):  # type: (str) -> Optional[StorageControllerCollection]
        if not isinstance(storage_id, str):
            raise TypeError("Storage id must be string. Received: {0}".format(type(storage_id)))
        ctrl_collection_path = "{0}/Storage/{1}/Controllers/".format(ROOT_ENDPOINT, storage_id)
        try:
            ctrl_collection_data = json.loads(self.get(ctrl_collection_path).read())
        except RESTClientNotFoundError:
            return None
        return StorageControllerCollection.from_json(self, ctrl_collection_data)

    def get_storage_controller(self, storage_id, storage_ctrl_id):  # type: (str, str) -> Optional[StorageController]
        if not isinstance(storage_id, str) or not isinstance(storage_ctrl_id, str):
            raise TypeError("Storage_id and storage_ctrl_id must be string. Received: {0} and {1}"
                            .format(type(storage_id), type(storage_ctrl_id)))
        try:
            data = json.loads(
                self.get("{0}/Storage/{1}/Controllers/{2}".format(ROOT_ENDPOINT, storage_id, storage_ctrl_id)).read())
            return StorageController.from_json(self, data)
        except RESTClientNotFoundError:
            return None

    def get_account_service(self):  # type: () -> AccountService
        account_service_path = "{0}/AccountService".format(ROOT_ENDPOINT)
        try:
            account_service_data = json.loads(self.get(account_service_path).read())
        except RESTClientNotFoundError:
            return None
        return AccountService.from_json(self, account_service_data)
