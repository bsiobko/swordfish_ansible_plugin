from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.manager.network_protocol import \
    ManagerNetworkProtocol


class Manager(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Manager, self).__init__(*args, **kwargs)

    @property
    def firmware_version(self):  # type: () -> str
        return self._get_field("FirmwareVersion")

    @property
    def service_entry_point_uuid(self):  # type: () -> str
        return self._get_field("ServiceEntryPointUUID")

    @property
    def uuid(self):  # type: () -> str
        return self._get_field("UUID")

    @property
    def power_state(self):  # type: () -> str
        return self._get_field("PowerState")

    @property
    def status(self):  # type: () -> Dict
        return self._get_field("Status")

    @property
    def graphical_console(self):  # type: () -> Dict
        return self._get_field("GraphicalConsole")

    @property
    def serial_console(self):  # type: () -> Dict
        return self._get_field("SerialConsole")

    @property
    def network_protocol(self):  # type: () -> Optional[ManagerNetworkProtocol]
        try:
            network_protocol_data = json.loads(self._client.get("{0}/NetworkProtocol".format(self._path)).read())
            return ManagerNetworkProtocol.from_json(self._client, network_protocol_data)
        except RESTClientNotFoundError:
            return None

    @property
    def ethernet_interface_collection(self):  # type: () -> List[EthernetInterface]
        NotImplementedError('Method not implemented!')

    @property
    def ethernet_interface(self, interface_id):  # type: (str) -> Optional[EthernetInterface]
        NotImplementedError('Method not implemented!')

    @property
    def virtual_media(self, vm_id):  # type: (str) -> Optional[VirtualMedia]
        NotImplementedError('Method not implemented!')

    def reset_graceful(self):  # type: () -> None
        self._client.post("{0}/Actions/Manager.Reset".format(self._path), body={
            "ResetType": "GracefulRestart",
        })

    def reset_force(self):  # type: () -> None
        self._client.post("{0}/Actions/Manager.Reset".format(self._path), body={
            "ResetType": "ForceRestart",
        })

    def reset_to_defaults(self, reset_type):  # type: (str) -> None
        if not isinstance(reset_type, str):
            raise TypeError("Reset_type must be string. Received: {0}".format(type(reset_type)))
        types = {
            "all": "ResetAll",
        }
        self._client.post("{0}/Actions/Manager.ResetToDefaults".format(self._path), body={
            "ResetToDefaultsType": types[reset_type],
        })
