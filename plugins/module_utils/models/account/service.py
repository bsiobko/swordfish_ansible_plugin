from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
try:
    from typing import Optional, ClassVar, List, Dict, Any
except ImportError:
    Optional = ClassVar = List = Dict = Any = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.account.account import Account
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.account.role import Role
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import (
    ACCOUNTS_ENDPOINT,
    ROLES_ENDPOINT,
    SIM_ACCOUNT_ENDPOINT,
    SIM_ROLE_ENDPOINT,
)


class AccountService(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(AccountService, self).__init__(*args, **kwargs)

    def create_account(self, username, password, role_id, enabled):  # type: (str, str, str, bool) -> None
        if not isinstance(username, str):
            raise TypeError("Username must be string. Received: {0}".format(type(username)))
        if not isinstance(password, str):
            raise TypeError("Password must be string. Received: {0}".format(type(password)))
        if not isinstance(role_id, str):
            raise TypeError("Role id must be string. Received: {0}".format(type(role_id)))
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))

        self._client.post(SIM_ACCOUNT_ENDPOINT.format(account=username), body={
            "UserName": username,
            "Password": password,
            "RoleId": role_id,
            "Enabled": enabled
        })
        self.reload()

    def get_account_collection(self):  # type: () -> List[Account]
        accounts = []
        accounts_collection = json.loads(self._client.get(ACCOUNTS_ENDPOINT).read()).get("Members")
        for member in accounts_collection:
            account_data = self._client.get(member["@odata.id"]).json
            accounts.append(Account.from_json(self._client, account_data))
        return accounts

    def get_role_collection(self):  # type: () -> List[Role]
        roles = []
        roles_collection = json.loads(self._client.get(ROLES_ENDPOINT).read()).get("Members")
        for member in roles_collection:
            role_data = self._client.get(member["@odata.id"]).json
            roles.append(Role.from_json(self._client, role_data))
        return roles

    def get_account(self, account_id):  # type: (str) -> Optional[Account]
        if not isinstance(account_id, str):
            raise TypeError("Account id must be string. Received: {0}".format(type(account_id)))
        try:
            account_data = json.loads(self._client.get(SIM_ACCOUNT_ENDPOINT.format(account=account_id)).read())
            return Account.from_json(self._client, account_data)
        except RESTClientNotFoundError:
            return None

    def get_role(self, role_id):  # type: (str) -> Optional[Role]
        if not isinstance(role_id, str):
            raise TypeError("Role id must be string. Received: {0}".format(type(role_id)))
        try:
            role_data = json.loads(self._client.get(SIM_ROLE_ENDPOINT.format(role=role_id)).read())
            return Role.from_json(self._client, role_data)
        except RESTClientNotFoundError:
            return None

    def delete_account(self, account_id):  # type: (str) -> None
        if not isinstance(account_id, str):
            raise TypeError("Account id must be string. Received: {0}".format(type(account_id)))
        self._client.delete(SIM_ACCOUNT_ENDPOINT.format(account=account_id))


class AccountServiceMockup(AccountService):

    def __init__(self, *args, **kwargs):
        super(AccountServiceMockup, self).__init__(*args, **kwargs)

    def create_account(self, username, password, role_id, enabled):  # type: (str, str, str, bool) -> None
        if not isinstance(username, str):
            raise TypeError("Username must be string. Received: {0}".format(type(username)))
        if not isinstance(password, str):
            raise TypeError("Password must be string. Received: {0}".format(type(password)))
        if not isinstance(role_id, str):
            raise TypeError("Role id must be string. Received: {0}".format(type(role_id)))
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))

        self._client.post(json.loads(self._client.get(ACCOUNTS_ENDPOINT).read()), body={
            "@odata.type": "ManagerAccount",
            "AccountTypes": [
                "Redfish"
            ],
            "Description": "User Account",
            "Id": username,
            "Enabled": enabled,
            "RoleId": role_id,
            "Links": {
                "Role": {
                    "@odata.id": json.loads(self._client.get(SIM_ROLE_ENDPOINT.format(role=role_id)).read())
                }
            },
            "Locked": False,
            "Locked@Redfish.AllowableValues": [
                "false"
            ],
            "Name": "User Account",
            "Password": password,
            "PasswordChangeRequired": False,
        })
        self.reload()
