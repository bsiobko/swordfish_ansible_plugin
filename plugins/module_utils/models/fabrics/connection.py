from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Connection(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Connection, self).__init__(*args, **kwargs)

    @property
    def description(self):  # type: () -> str
        return self._get_field("Description")

    def set_description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def connection_type(self):  # type: () -> str
        return self._get_field("ConnectionType")

    def set_connection_type(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New connection type must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"ConnectionType": new})
        self.reload()

    @property
    def volume_info(self):  # type: () -> list
        return self._get_field("VolumeInfo")

    def set_volume_info(self, new):  # type: (list) -> None
        if not isinstance(new, list):
            raise TypeError("New volume_info must be list. Received: {0}".format(type(new)))
        for i in new:
            if not isinstance(i, dict):
                raise TypeError("Items of new volume_info must be dicts. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"VolumeInfo": new})
        self.reload()

    @property
    def links(self):  # type: () -> dict
        return self._get_field("Links")

    def set_links(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New links must be dict. Received: {0}".format(type(new)))
        for i in new.items():
            if not isinstance(i[1], list):
                raise TypeError("Items of new links must be list. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"Links": new})
        self.reload()

    @property
    def name(self):  # type: () -> str
        return self.get_name()

    def set_name(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New name must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Name": new})
        self.reload()
