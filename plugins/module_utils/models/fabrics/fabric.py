from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.connection import Connection
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError


class Fabric(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Fabric, self).__init__(*args, **kwargs)

    @property
    def description(self):  # type: () -> str
        return self._get_field("Description")

    @property
    def fabric_type(self):  # type: () -> str
        return self._get_field("FabricType")

    @property
    def status(self):  # type: () -> dict
        return self._get_field("Status")

    @property
    def volume_info(self):  # type: () -> list
        return self._get_field("VolumeInfo")

    @property
    def links(self):  # type: () -> list
        return self._get_field("Links")

    @property
    def initiator_endpoints(self):  # type: () -> list
        try:
            return self._data["Links"]["InitiatorEndpoints"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][InitiatorEndpoints]")

    @property
    def target_endpoints(self):  # type: () -> list
        try:
            return self._data["Links"]["TargetEndpoints"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][TargetEndpoints]")

    def get_connection(self, connection_id):  # type: (str) -> Optional[Connection]
        if not isinstance(connection_id, str):
            raise TypeError("Connection_id must be string. Received: {0}".format(type(connection_id)))
        try:
            connection_data = json.loads(
                self._client.get("{0}/Connections/{1}".format(self._path, connection_id)).read())
            return Connection.from_json(self._client, connection_data)
        except RESTClientNotFoundError:
            return None

    def create_connection(self, data, connection_id):  # type: (dict, str) -> None
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if not isinstance(connection_id, str):
            raise TypeError("Connection_id must be str. Received: {0}".format(type(connection_id)))
        path = "{0}/Connections/{1}".format(self._path, connection_id)
        self._client.post(path, body=data)

    def delete_connection(self, connection_id):  # type: (str) -> None
        if not isinstance(connection_id, str):
            raise TypeError("Data must be str. Received: {0}".format(type(connection_id)))
        path = "{0}/Connections/{1}".format(self._path, connection_id)
        self._client.delete(path)
