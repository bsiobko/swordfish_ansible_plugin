from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError, \
    RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
import json

try:
    from typing import List, Optional
except ImportError:
    List = None


class StoragePool(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(StoragePool, self).__init__(*args, **kwargs)

    @property
    def actions(self):  # type: () -> list
        return self._get_field("Actions")

    @property
    def odata_id(self):  # type: () -> str
        return self._get_field("@odata.id")

    @property
    def allocated_pools(self):  # type: () -> list
        return self._get_field("AllocatedPools")

    @property
    def allocated_volumes(self):  # type: () -> list
        return self._get_field("AllocatedVolumes")

    @property
    def block_size_bytes(self):  # type: () -> int
        return self._get_field("BlockSizeBytes")

    @property
    def capacity(self):  # type: () -> dict
        return self._get_field("Capacity")

    def set_capacity(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New capacity must be dict. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Capacity": new})
        self.reload()

    @property
    def capacity_sources(self):
        return self._get_field("CapacitySources")

    @property
    def class_of_service(self):
        return self._get_field("ClassesOfService")

    @property
    def compressed(self):  # type: () -> bool
        return self._get_field("Compressed")

    @property
    def compression_enabled(self):  # type: () -> bool
        return self._get_field("CompressionEnabled")

    @property
    def deduplicated(self):
        return self._get_field("Deduplicated")

    @property
    def deduplication_enabled(self):
        return self._get_field("DeduplicationEnabled")

    @property
    def default_class_of_service(self):
        return self._get_field("DefaultClassOfService")

    @property
    def default_compression_behavior(self):
        return self._get_field("DefaultCompressionBehavior")

    @property
    def default_deduplication_behavior(self):
        return self._get_field("DefaultDeduplicationBehavior")

    @property
    def default_encryption_behavior(self):
        return self._get_field("DefaultEncryptionBehavior")

    @property
    def description(self):  # type: () -> str
        return self._data.get("Description")

    def set_description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def encrypted(self):
        return self._get_field("Encrypted")

    @property
    def encryption_enabled(self):
        return self._get_field("EncryptionEnabled")

    @property
    def id(self):  # type: () -> str
        return self.get_id()

    @property
    def identifier(self):
        return self._get_field("Identifier")

    @property
    def io_statistics(self):
        return self._get_field("IOStatistics")

    @property
    def name(self):  # type: () -> str
        return self.get_name()

    def set_name(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New name must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Name": new})
        self.reload()

    def get_allocated_volume(self, volume_id):  # type: (str) -> Optional[Volume]
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be str. Received: {0}".format(type(str)))
        try:
            volume = json.loads(self._client.get("{0}/AllocatedVolumes/{1}".format(self._path, volume_id)).read())
            return Volume.from_json(self._client, volume)
        except RESTClientNotFoundError:
            return None

    def create_allocated_volume(self, data):  # type: (dict) -> None
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if data.get("Name") is None:
            raise SwordfishFieldNotFoundError("Name")
        path = "{0}/AllocatedVolumes/{1}".format(self._path, data["Name"])
        self._client.post(path, body=data)

    def delete_allocated_volume(self, volume_id):  # type: (str) -> None
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be string. Received: {0}".format(type(volume_id)))
        path = "{0}/AllocatedVolumes/{1}".format(self._path, volume_id)
        self._client.delete(path)
