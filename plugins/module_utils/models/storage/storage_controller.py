from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class StorageController(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(StorageController, self).__init__(*args, **kwargs)

    @property
    def status(self):   # type: () -> dict
        return self._get_field("Status")

    @property
    def manufacturer(self):  # type: () -> str
        return self._get_field("Manufacturer")

    @property
    def model(self):  # type: () -> str
        return self._get_field("Model")

    @property
    def serial_number(self):  # type: () -> str
        return self._get_field("SerialNumber")

    @property
    def part_number(self):  # type: () -> str
        return self._get_field("PartNumber")

    @property
    def firmware_version(self):  # type: () -> str
        return self._get_field("FirmwareVersion")

    @property
    def supported_controller_protocols(self):  # type: () -> list
        return self._get_field("SupportedControllerProtocols")

    @property
    def supported_device_protocols(self):  # type: () -> list
        return self._get_field("SupportedDeviceProtocols")

    @property
    def nvme_controller_properties(self):   # type: () -> dict
        return self._get_field("NVMeControllerProperties")

    @property
    def links(self):    # type: () -> dict
        return self._get_field("Links")

    def attach_namespace(self, namespace_urls):  # type: (list) -> None
        if not isinstance(namespace_urls, list):
            raise TypeError("Namespace_urls must be list. Received: {0}".format(type(namespace_urls)))
        links = {"Links": self.links}
        attached_volumes = links["Links"]["AttachedVolumes"]
        for namespace in namespace_urls:
            if {"@odata.id": namespace} in attached_volumes:
                raise ValueError("{0} already attached".format(namespace))
            attached_volumes.append({"@odata.id": namespace})
        self._client.patch(self._path, body=links)
        self.reload()

    def detach_namespace(self, namespace_urls):  # type: (list) -> None
        if not isinstance(namespace_urls, list):
            raise TypeError("Namespace_urls must be list. Received: {0}".format(type(namespace_urls)))
        links = {"Links": self.links}
        attached_volumes = links["Links"]["AttachedVolumes"]
        if len(namespace_urls) == 0:
            attached_volumes.clear()
        for namespace in namespace_urls:
            if {"@odata.id": namespace} not in attached_volumes:
                raise ValueError("{0} not attached".format(namespace))
            attached_volumes.remove({"@odata.id": namespace})
        self._client.patch(self._path, body=links)
        self.reload()
