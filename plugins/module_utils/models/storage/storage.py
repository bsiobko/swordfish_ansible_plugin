from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError, \
    RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_POOLS_ENDPOINT, \
    STORAGE_VOLUMES_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool


class Storage(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Storage, self).__init__(*args, **kwargs)

    def get_storage_pools(self):
        pools = []
        if not self._data.get("StoragePool"):
            return pools

        storage_pools_data = json.loads(self._client.get(STORAGE_POOLS_ENDPOINT
                                                         .format(path_to_storage=self._path)).read())['Members']
        for pool in storage_pools_data:
            pools.append(StoragePool(client=self._client, path=pool['@odata.id']))
        return pools

    def get_storage_pool(self, storage_pool_id):  # type: (str) -> Optional[StoragePool]
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        try:
            storage_pool_data = json.loads(
                self._client.get("{0}/StoragePools/{1}".format(self._path, storage_pool_id)).read())
            return StoragePool.from_json(self._client, storage_pool_data)
        except RESTClientNotFoundError:
            return None

    def get_namespace(self, namespace_id):  # type: (str) -> Optional[Namespace]
        if not isinstance(namespace_id, str):
            raise TypeError("Namespace_id must be string. Received: {0}".format(type(namespace_id)))
        try:
            namespace_data = json.loads(
                self._client.get("{0}/Volumes/{1}".format(self._path, namespace_id)).read())
            return Namespace.from_json(self._client, namespace_data)
        except RESTClientNotFoundError:
            return None

    def delete_storage_pool(self, storage_pool_id):  # type: (str) -> None
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        path = "{0}/StoragePools/{1}".format(self._path, storage_pool_id)
        self._client.delete(path)

    def create_storage_pool(self, data):
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if data.get("Name") is None:
            raise SwordfishFieldNotFoundError("Name")
        path = "{0}/StoragePools/{1}".format(self._path, data["Name"])
        self._client.post(path, body=data)

    def patch_volume(self, volume_id, data):
        path = "{0}/Volumes/{1}".format(self._path, volume_id)
        return self._client.patch(path, body=data)

    def create_volume(self, volume_id, data):
        path = "{0}/Volumes/{1}".format(self._path, volume_id)
        if volume_id is None:
            path = "{0}/Volumes".format(self._path)
        return self._client.post(path, body=data)

    def delete_volume(self, volume_id):
        path = "{0}/Volumes/{1}".format(self._path, volume_id)
        return self._client.delete(path)

    def get_volume(self, volume_id):  # type: (str) -> Volume
        volume_data = json.loads(
            self._client.get("{0}/Volumes/{1}".format(self._path, volume_id)).read())
        return Volume.from_json(self._client, volume_data)

    def get_volumes(self):
        volumes = []
        if not self._data.get("Volumes"):
            return volumes

        volumes_collection_data = json.loads(self._client.get("{path}/Volumes"
                                                              .format(path=self._path)).read())['Members']

        for volume in volumes_collection_data:
            volumes.append(Volume(client=self._client, path=volume['@odata.id']))
        return volumes_collection_data
