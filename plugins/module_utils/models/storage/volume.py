from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Volume(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Volume, self).__init__(*args, **kwargs)

    @property
    def capacity_bytes(self):  # type: () -> int
        return self._get_field("CapacityBytes")

    def set_capacity_bytes(self, new):  # type: (int) -> None
        if not isinstance(new, int):
            raise TypeError("New capacity bytes must be int. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"CapacityBytes": new})
        self.reload()

    @property
    def status(self):  # type: () -> dict
        return self._get_field("Status")

    def set_status(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New status must be dict. Received: {0}".format(type(new)))
        for i in new.items():
            if not isinstance(i[1], str):
                raise TypeError("New status items must be str. Received: {0}".format(type(i[1])))
        self._client.patch(self._path, body={"Status": new})
        self.reload()

    @property
    def identifiers(self):  # type: () -> Optional[list]
        return self._get_field("Identifiers")

    def set_identifiers(self, new):  # type: (list) -> None
        if not isinstance(new, list):
            raise TypeError("New identifiers must be list. Received: {0}".format(type(new)))
        for i in new:
            if not isinstance(i, str):
                raise TypeError("New identifiers items must be dict. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"Identifiers": new})
        self.reload()

    @property
    def description(self):  # type: () -> Optional[dict]
        return self._get_field("Description")

    def set_description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def raid_type(self):  # type: () -> Optional[str]
        return self._get_field("RAIDType")

    def set_raid_type(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New RAID type must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"RAIDType": new})
        self.reload()
