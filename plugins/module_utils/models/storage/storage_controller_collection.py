from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class StorageControllerCollection(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(StorageControllerCollection, self).__init__(*args, **kwargs)

    @property
    def members(self):  # type: () -> dict
        return self._get_field("Members")

    def get_controller(self, controller_id):  # type: (str) -> Optional[StorageController]
        if not isinstance(controller_id, str):
            raise TypeError("Controller_id must be string. Received: {0}".format(type(controller_id)))
        try:
            data = json.loads(
                self._client.get("{0}/Controllers/{1}".format(self._path, controller_id)).read())
            return StorageController.from_json(self._client, data)
        except RESTClientNotFoundError:
            return None
