#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r'''
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import ROOT_ENDPOINT


class PingModule(SwordfishModule):

    def __init__(self):
        super(PingModule, self).__init__(
            supports_check_mode=True
        )

    def run(self):
        self.rest.get(ROOT_ENDPOINT)
        self.exit_json(
            msg="Operation successful.",
            changed=False,
        )


def main():
    PingModule()


if __name__ == '__main__':
    main()
