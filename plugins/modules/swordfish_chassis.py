from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json
from functools import partial

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import CHASSIS_ENDPOINT, build_url
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError

DOCUMENTATION = r'''
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class SwordfishChassisModule(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'required': False,
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'id': {
                'type': 'str',
                'required': False,
            },
            'body': {
                'type': 'dict',
                'required': False,
                'default': {
                    'Manufacturer': 'NVMeDriveVendorFoo'
                },
            },

        }
        super(SwordfishChassisModule, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        upd_params = {}
        action = None
        chassis_exists = False

        if self.params['id']:
            chassis_exists = True
            try:
                self.rest.get(build_url(CHASSIS_ENDPOINT, self.params['id']))
            except RESTClientError as e:
                pass

        if self.params['state'] == 'present':
            if chassis_exists:
                upd_params["path"] = build_url(CHASSIS_ENDPOINT, self.params['id'])
                upd_params["body"] = self.params['body']
                action = partial(self.rest.patch, **upd_params)
                # self.rest.patch(CHASSIS_ENDPOINT + '/' + self.params['id'], body=self.params['body'])
            else:
                upd_params["path"] = CHASSIS_ENDPOINT
                upd_params["body"] = self.params['body']
                if self.params['id']:
                    self.params['body']["@odata.id"] = build_url(CHASSIS_ENDPOINT, self.params['id'])

                action = partial(self.rest.post, **upd_params)
                # resp = json.loads(self.rest.post(CHASSIS_ENDPOINT, body=self.params['body']).read())
        else:  # absent
            upd_params["path"] = build_url(CHASSIS_ENDPOINT, self.params['id'])
            action = partial(self.rest.delete, **upd_params)

            # self.rest.delete(CHASSIS_ENDPOINT + f"/{self.params['id']}")
        if not action:
            self.exit_json(msg='No changes required', changed=False)

        resp = json.loads(action().read())

        self.exit_json(
            msg="Operation successful.",
            resp=resp,
            changed=False,
        )


def main():
    SwordfishChassisModule()


if __name__ == '__main__':
    main()
