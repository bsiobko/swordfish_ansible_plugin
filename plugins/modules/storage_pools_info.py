from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

DOCUMENTATION = r"""
---
module: storage_pools_info
short_description: Get information about configured pools.
"""

RETURN = r"""
"""

EXAMPLES = r"""
"""

import json
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage


class StoragePoolsInfo(SwordfishModule):
    def __init__(self):
        super(StoragePoolsInfo, self).__init__(
            supports_check_mode=True
        )

    def run(self):
        storage_members = json.loads(self.rest.get(STORAGE_ENDPOINT).read()).get("Members")
        pools = []
        for member in storage_members:
            storage = Storage(client=self.rest, path=member['@odata.id'])
            pools.append(
                {
                    'storage': storage.get_path(),
                    'storage_pools': [{
                        '@odata.id': pool.odata_id,
                        'name': pool.name,
                        'id': pool.id,
                        'description': pool.description,
                    } for pool in storage.get_storage_pools()]
                }
            )
        self.exit_json(
            msg="Operation successful.",
            pools_info=pools,
            changed=False
        )


def main():
    StoragePoolsInfo()


if __name__ == "__main__":
    main()
