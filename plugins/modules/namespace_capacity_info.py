#!/usr/bin/env python3
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import NAMESPACE_ENDPOINT

DOCUMENTATION = r'''
module: namespace_capacity_info
short_description: Get information about namespace capacity
description:
  - This module is intended to get information about
   namespace capacity
options:
  drive_id:
    type: str
    required: True
  namespace:
    type: str
    required: True
'''

EXAMPLES = r'''
- name: Test get namespace capacity | Get namespace capacity
  spbstu.swordfish.namespace_capacity_info:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    drive_id: "IPAttachedDrive1"
    namespace: "SimpleNamespace"
'''

RETURN = r'''
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
allocated_bytes:
  type: int
  returned: on success
consumed_bytes:
  type: int
  returned: on success
'''

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class NamespaceCapacityInfo(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'drive_id': {
                'type': 'str',
                'required': True,
            },
            'namespace': {
                'type': 'str',
                'required': True,
            },

        }
        super(NamespaceCapacityInfo, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        storage = self.rest.get_storage(self.params["drive_id"])
        namespace = storage.get_namespace(self.params["namespace"])
        capacity = namespace.capacity
        self.exit_json(
            msg="Operation successful.",
            changed=False,
            allocated_bytes=capacity["Data"].get("AllocatedBytes"),
            consumed_bytes=capacity["Data"].get("ConsumedBytes"),
        )


def main():
    NamespaceCapacityInfo()


if __name__ == '__main__':
    main()
