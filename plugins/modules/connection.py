from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_ENDPOINT, FABRICS_ENDPOINT

DOCUMENTATION = r"""
module: connection
short_description: Create a new connection to an existing Volume, using a pre-existing Endpoint
to grant read/write access to the Volume.
description:
  - This module supports check mode.
options:
  fabric_id:
    type: str
    required: True
  storage_id:
    type: str
    required: True
  volume_id:
    type: str
    required: False
  description:
    type: str
    required: False
  name:
    type: str
    required: False
  initiator_endpoints_id:
    required: False
    type: list
    elements: str
  target_endpoints_id:
    required: False
    type: list
    elements: str
  access_capabilities:
    required: False
    type: list
    elements: str
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) creates a new connection if I(storage_id) does not exists.
      - C(absent) deletes an existing connection.
"""

RETURN = r"""
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
- name: Test connection creation | Create connection
  spbstu.swordfish.connection:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    fabric_id: "NVMeoF"
    volume_id: "SimpleNamespace"
    storage_id: "IPAttachedDrive1"
    description: "Description"
    name: "ConnectionTest"
    initiator_endpoints_id:
      - Initiator1
      - Initiator2
    target_endpoints_id:
      - D1-E1
      - D1-E2

- name: Test connection changing | Change target endpoints of ConnectionTest
  spbstu.swordfish.connection:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    fabric_id: "NVMeoF"
    volume_id: "SimpleNamespace"
    storage_id: "IPAttachedDrive1"
    description: "Description"
    name: "ConnectionTest"
    initiator_endpoints_id:
      - Initiator1
      - Initiator2
    target_endpoints_id:
      - D1-E1

- name: Test connection changing | Change initiator endpoints of ConnectionTest
  spbstu.swordfish.connection:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    fabric_id: "NVMeoF"
    volume_id: "SimpleNamespace"
    storage_id: "IPAttachedDrive1"
    description: "Description"
    name: "ConnectionTest"
    initiator_endpoints_id:
      - Initiator1
    target_endpoints_id:
      - D1-E1

- name: Test connection changing | Change description of ConnectionTest
  spbstu.swordfish.connection:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    fabric_id: "NVMeoF"
    volume_id: "SimpleNamespace"
    storage_id: "IPAttachedDrive1"
    description: "Another description"
    name: "ConnectionTest"
    initiator_endpoints_id:
      - Initiator1
      - Initiator2
    target_endpoints_id:
      - D1-E1

- name: Test connection deleting | Delete ConnectionTest
  spbstu.swordfish.connection:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    fabric_id: "NVMeoF"
    volume_id: "SimpleNamespace"
    storage_id: "IPAttachedDrive1"
    name: "ConnectionTest"
    state: "absent"
"""

from functools import partial
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class Connection(SwordfishModule):

    def __init__(self):
        argument_spec = {
            "fabric_id": {"type": "str", "required": True},
            "storage_id": {"type": "str", "required": True},
            "volume_id": {"type": "str", "required": True},
            "description": {"type": "str", "required": False},
            "name": {"type": "str", "required": True},
            "initiator_endpoints_id": {"type": "list", "elements": "str", "required": False},
            "target_endpoints_id": {"type": "list", "elements": "str", "required": False},
            "access_capabilities": {"type": "list", "elements": "str", "required": False},
            "state": {"type": "str", "default": "present", "choices": ["present", "absent"], "required": False},
        }
        super(Connection, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True,
            required_if=[('state', 'present', ('volume_id',))],
        )

    def run(self):
        fabric = self.rest.get_fabric(self.params["fabric_id"])
        action = []
        connection = fabric.get_connection(self.params["name"])
        if self.params["state"] == "present":
            data = dict()
            data["Name"] = self.params["name"]
            if self.params["description"] is not None:
                data["Description"] = self.params["description"]

            volume_info = list()
            volume_info_data = dict()
            volume_info_data["AccessCapabilities"] = self.params["access_capabilities"]
            volume_info_data_odata_id = dict()
            volume_info_data_odata_id["@odata_id"] = "/{0}/{1}/Volumes/{2}".format(
                STORAGE_ENDPOINT,
                self.params["storage_id"],
                self.params["volume_id"]
            )
            volume_info_data["Volume"] = volume_info_data_odata_id
            volume_info.append(volume_info_data)
            data["VolumeInfo"] = volume_info

            links_dict = dict()

            if self.params["initiator_endpoints_id"] is not None:
                links_initiator_list = list()
                for initiator_id in self.params["initiator_endpoints_id"]:
                    links_initiator_data = dict()
                    links_initiator_data["@odata.id"] = "/{0}/{1}/Endpoints/{2}".format(
                        FABRICS_ENDPOINT,
                        self.params["fabric_id"],
                        initiator_id
                    )
                    links_initiator_list.append(links_initiator_data)
                links_dict["InitiatorEndpoints"] = list(links_initiator_list)

            if self.params["target_endpoints_id"] is not None:
                links_target_list = list()
                for target_id in self.params["target_endpoints_id"]:
                    links_targets_data = dict()
                    links_targets_data["@odata.id"] = "/{0}/{1}/Endpoints/{2}".format(
                        FABRICS_ENDPOINT,
                        self.params["fabric_id"],
                        target_id
                    )
                    links_target_list.append(links_targets_data)
                links_dict["TargetEndpoints"] = list(links_target_list)

            if links_dict:
                data["Links"] = links_dict

            if connection:
                if "Links" in data and (not connection.contains_field("Links") or
                                        connection.contains_field("Links") and data["Links"] != connection.links):
                    action.append(partial(connection.set_links, data["Links"]))
                if "VolumeInfo" in data and (not connection.contains_field("VolumeInfo") or
                                             connection.contains_field("VolumeInfo") and
                                             data["VolumeInfo"] != connection.volume_info):
                    action.append(partial(connection.set_volume_info, data["VolumeInfo"]))
                if "Description" in data and (not connection.contains_field("Description") or
                                              connection.contains_field("Description") and
                                              data["Description"] != connection.description):
                    action.append(partial(connection.set_description, data["Description"]))
            else:
                action.append(partial(fabric.create_connection, data, self.params['name']))
        else:
            if connection:
                action.append(partial(fabric.delete_connection, self.params['name']))

        if not self.check_mode:
            for item in action:
                item()
        if action:
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    Connection()


if __name__ == "__main__":
    main()
