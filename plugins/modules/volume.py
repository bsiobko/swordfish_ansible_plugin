from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError, RESTClientNotFoundError

from functools import partial
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class Volume(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'required': False,
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'volume_id': {
                'type': 'str',
                'required': False,
            },
            "storage_id": {"type": "str", "required": True},
            'body': {
                'type': 'dict',
                'required': False,
                'default': {
                    'Manufacturer': 'NVMeDriveVendorFoo'
                },
            },
            "description": {"type": "str", "required": False},
            "capacity_bytes": {"type": "int", "required": False},
            "raid_type": {"type": "str", "required": False},
        }
        super(Volume, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        action = None
        volume_exists = False
        has_id = self.params['volume_id'] is not None

        storage = self.rest.get_storage(self.params["storage_id"])

        if has_id:
            try:
                storage.get_volume(self.params["volume_id"])
                volume_exists = True
            except (RESTClientError, RESTClientNotFoundError) as e:
                pass

        if self.params['state'] == 'present':
            if volume_exists:
                if self.params['body']:
                    action = partial(storage.patch_volume, volume_id=self.params['volume_id'], data=self.params['body'])
            else:
                action = partial(storage.create_volume, volume_id=self.params['volume_id'], data=self.params['body'])
        else:  # absent
            if volume_exists:
                action = partial(storage.delete_volume, volume_id=self.params['volume_id'])

        if not action:
            self.exit_json(msg='No changes required', changed=False)

        resp = None
        if not self.check_mode:
            resp = json.loads(action().read())

        self.exit_json(
            msg="Operation successful.",
            resp=resp,
            changed=True,
        )


def main():
    Volume()


if __name__ == "__main__":
    main()
