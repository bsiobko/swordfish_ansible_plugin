from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
---
module: namespace_can_be_created
short_description: Get information if namespace can be created.
version_added: "1.0.0"
description:
  - This module get information if namespace can be created within specified Storage
  - Supports check mode
extends_documentation_fragment:
  - spbstu.swordfish.connection_options
options:
  storage_id:
    type: str
    required: True
    description: Id of Storage
"""

RETURN = r"""
---
msg:
  type: str
  returned: always
  description: Operation status message
error:
  type: str
  returned: on error
  description: Error details if raised
can_be_created:
  type: bool
  returned: always
  description: Says if namespace can be created
"""

EXAMPLES = r"""
---
- name: Test if namespace can be created | Get info
  spbstu.swordfish.namespace_can_be_created:
    connection:
      base_url: "{{ base_url }}",
      username: "Administrator",
      password: "Password"
    storage_id: "IPAttachedDrive1"
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_VOLUMES_ENDPOINT


class NamespaceCanBeCreated(SwordfishModule):
    def __init__(self):
        argument_spec = {
            "storage_id": {"type": "str", "required": True}
        }
        super(NamespaceCanBeCreated, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        response = self.rest.get(STORAGE_VOLUMES_ENDPOINT.format(storage_id=self.params["storage_id"]))
        result = (response.status == 200) and bool(response.headers["Allow"])
        self.exit_json(
            msg="Operation successful.",
            can_be_created=result,
            changed=False
        )


def main():
    NamespaceCanBeCreated()


if __name__ == "__main__":
    main()
